import axios from "axios";
import { message } from "ant-design-vue";
import CODEMAP from "./coodMap";
export interface PageNationParams {
  page?: number;
  pageSize?: number;
}

export interface Params extends PageNationParams {
  [propertyName: string]: string | number | boolean | undefined;
}
// console.log(process.env);
const httpTool = axios.create({
  timeout: 10000,
  baseURL: process.env.VUE_APP_BASEURL, // 开发环境接口  生产接口
});
// 响应拦截 对接口返回值做统一处理，比如状态码错误提示，如果统一包装返回值结果
httpTool.interceptors.response.use(
  (response) => {
    // console.log("interceptors.response:", response)
    if (response.data?.success) {
      return response.data;
    }
    message.error(
      response.data?.msg || CODEMAP[response.data?.statusCode || 500]
    );
    return response;
  },
  (error) => {
    // 非 200~300  非 304
    if (error.code === "ECONNABORTED") {
      // 网络超时
      message.error("您当前网络环境不好，请刷新重试~");
    } else {
      message.error(
        error.response?.statusText ||
          CODEMAP[error.response.data?.status || 400]
      );
    }
    return Promise.reject(error);
  }
);

export default {
  ...httpTool,
  get(url: string, params: Params) {
    return httpTool.get(url, {
      params,
    });
  },
  post() {
    console.log("post");
  },
};
